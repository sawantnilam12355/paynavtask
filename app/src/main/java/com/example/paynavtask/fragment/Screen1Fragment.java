package com.example.paynavtask.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.paynavtask.R;
import com.google.android.material.imageview.ShapeableImageView;

public class Screen1Fragment extends Fragment implements View.OnClickListener {
    private View rootview;
    ConstraintLayout Main_Constraint,your_accountView,constraintLayout3;
    ShapeableImageView shapeableImageView5,shapeableImageView6;
    TextView tv_recent,tv_text6;
    Animation slide_down,slide_up;
    ImageView img_your_acct;

    public Screen1Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_screen1, container, false);

        initializeview(rootview);

        //Load animation
        slide_down = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down);
        slide_up = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_up);

        shapeableImageView5.setOnClickListener(this);
        img_your_acct.setOnClickListener(this);

        return rootview;
    }

    private void initializeview(View rootview) {
        Main_Constraint =rootview.findViewById(R.id.Main_Constraint);
        shapeableImageView5 =rootview.findViewById(R.id.shapeableImageView5);
        your_accountView =rootview.findViewById(R.id.your_accountView);
        img_your_acct =rootview.findViewById(R.id.img_your_acct);
        shapeableImageView6 =rootview.findViewById(R.id.shapeableImageView6);
        tv_recent =rootview.findViewById(R.id.tv_recent);
        tv_text6 =rootview.findViewById(R.id.tv_text6);
        constraintLayout3 =rootview.findViewById(R.id.constraintLayout3);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.shapeableImageView5:
                Main_Constraint.startAnimation(slide_down);
                Main_Constraint.setVisibility(View.GONE);
                your_accountView.setVisibility(View.VISIBLE);
                shapeableImageView5.setVisibility(View.GONE);
                tv_recent.setVisibility(View.GONE);
                shapeableImageView6.setVisibility(View.VISIBLE);
                tv_text6.setVisibility(View.VISIBLE);
                constraintLayout3.setVisibility(View.VISIBLE);
                break;
            case R.id.img_your_acct:
                Main_Constraint.startAnimation(slide_up);
                Main_Constraint.setVisibility(View.VISIBLE);
                your_accountView.setVisibility(View.GONE);
                shapeableImageView5.setVisibility(View.VISIBLE);
                tv_recent.setVisibility(View.VISIBLE);
                shapeableImageView6.setVisibility(View.GONE);
                tv_text6.setVisibility(View.GONE);
                constraintLayout3.setVisibility(View.GONE);
                break;
        }
    }
}